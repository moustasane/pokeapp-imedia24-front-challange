

export function Loading(props) {

  return (
    <>

    {
        props.loading ? (
        
        
            <><div  data-test-id="loadingContainer" className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                      <div role="status">
                        <img className="animate-spin" src="logo192.png"/>
                          <span > Loading... </span>
                      </div>
                  </div><div className="opacity-25 fixed inset-0 z-40 bg-black"></div></>
        
        ) : null
    }

    </>
  );
}
