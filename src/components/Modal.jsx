import { AnyAction } from "@reduxjs/toolkit";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setShowModal } from "./PokeListSlice";

export function Modal() {
  const dispatch = useDispatch();
  const currentPoke = useSelector((state) => state.pokemones.currentPoke);
  const showModal = useSelector((state) => state.pokemones.showModal);

   const closeModal = () => {


    dispatch(setShowModal(false))

  }
   
  return (
    <>

{showModal && currentPoke ? (
        <>
          <div
            className="poke-details-modal justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
          >
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                  <h3 className="text-3xl font-semibold capitalize">
                    { currentPoke.name}
                  </h3>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => closeModal()}
                  >
                    <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div class="max-w-sm rounded overflow-hidden shadow-lg">
  <img class="w-full" src={currentPoke.sprites.back_default} alt="Sunset in the mountains"/>
  <div class="px-4 py-1">
    <div class="font-bold text-xl mb-2">Abilities : </div>
  </div>
  <div class="px-6 pt-2 pb-0">

    {

      currentPoke.abilities.map((ab)=>(
       
       <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#{ab.ability.name}</span>
      
       ))
    }
   
 
  </div>
</div>
                {/*footer*/}
                <div className="flex items-center justify-end p-3 bg-white border-t border-solid border-slate-200 rounded-b">
                  <button
                    className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => closeModal()}
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}

    </>
  );
}
