import  { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import { getCurrentPoke, getPokeList, setLoading, setShowModal } from "./PokeListSlice";
import { Loading } from "./Loading";


export function PokeListDetails(){

const dispatch = useDispatch();
const cards = useSelector((state) => state.pokemones.pokeCollection);
const hasMore = useSelector((state) => state.pokemones.hasMore);
const [page, setPage] = useState(0);
const _loading = useSelector((state) => state.pokemones.loading);
const openModal = async (poke) => {
  await dispatch(getCurrentPoke(poke))
  dispatch(setShowModal(true))
};

useEffect(() => {
  dispatch(getPokeList(0));
  setPage((prevState) => {
    return prevState + 1;
  });
}, [dispatch]);

const ftechPokeList = () => {
  setPage((prevState) => prevState + 1);
  dispatch(setLoading())
  setTimeout(() => {
    if (hasMore) {
      dispatch(getPokeList(page));
    }
  }, 500);
};

const cardsDetails = cards ? (
  <InfiniteScroll
    next={ftechPokeList}
    hasMore={hasMore}
    dataLength={cards.length}
    loader={<Loading loading={_loading}/>}
  >
    <div id="dataContainer" class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:gap-4">
      {cards.map((card) => (
        <div onClick={()=> openModal(card.url)} className="m-2 p-4 border-2 cursor-pointer  rounded-lg shadow-lg hover:bg-gray-500 hover:text-white transition duration-150 ease-out hover:ease-in capitalize">
          {card.name}
        </div>
      ))}
    </div>
  </InfiniteScroll>
) : (
  null
);


return(
    cardsDetails
)
}

