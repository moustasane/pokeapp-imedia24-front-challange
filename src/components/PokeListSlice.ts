import axios  from "axios"

const initialState = {
  pokeCollection: [],
  currentPoke : null,
  hasMore: true,
  showModal : false,
  loading: true,
};

export default function PokeListReducer(state = initialState, action: any) {
  switch (action.type) {
    case "getPokeList": {
      let setHasMore;
      if (action.payload !== undefined) {
        setHasMore = action.payload.length !== 0 ? true : false;
      } else {
        setHasMore = false;
      }

      return {
        ...state,
        hasMore: setHasMore,
        loading : false,    
        pokeCollection:
          state.hasMore === true
            ? state.pokeCollection.concat(action.payload)
            : state.pokeCollection,
      };
    }


    case 'setLoading' : {
              return {
            ...state,
            loading : true }   

    }

    
    case 'getCurrentPoke' :  {  

      return {
        ...state,
        currentPoke : action.payload }   
    }
    case 'setShowModal' :  {  
        console.log(action.payload);
        
      return {
        ...state,
        showModal : action.payload }   
    }

    default:
      return state;
  }
}

export const getPokeList = (page: number) => async (dispatcher: any) => {
    console.log("page=",page);
    
    const params = { offset : page*50, limit: 50 };

    const resp = await axios.get("https://pokeapi.co/api/v2/pokemon/", { params });

    if (resp) {
      dispatcher({ type: "getPokeList", payload: resp.data.results });
    }
  };


  export const getCurrentPoke = (url: string) => async (dispatcher: any) => {    
    
      
    const resp = await axios.get(url);

    if (resp) {
      dispatcher({ type: "getCurrentPoke", payload: resp.data });
    }
  };
export const setLoading = () =>  (dispatcher: any) => {

      dispatcher({ type: "setLoading", payload: "" });
};
export const setShowModal = (show:Boolean) =>  (dispatcher: any) => {

      dispatcher({ type: "setShowModal", payload: show});
  };


