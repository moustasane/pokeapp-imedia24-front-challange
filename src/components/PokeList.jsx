


import { PokeListDetails } from "./PokeListDetails";

export function PokeList() {


 
  return (
    <>
      <div className="relative bg-white sticky top-0">
        <div className="mx-auto max-w-7xl px-4 sm:px-6">
          <div className="flex items-center justify-between border-b-2 border-gray-100 py-6 ">
            <div className="flex justify-center lg:w-0 lg:flex-1">
              <a href="#">
                <span className="sr-only">Pokemones</span>
                <img
                  className="h-8 w-auto m-auto"
                  src="https://pokemon.images.gamespress.com/Content/Artwork/NickNack/PokemonAmerica/artwork/2019/07/09125735-7b00e266-d991-41da-9267-843e49ce62a7/Pokemon_Logo.jpg?w=240&mode=max&otf=y&quality=90&format=jpg&bgcolor=white&ex=2023-02-01+03%3A00%3A00&sky=e8a969af197332ce0079c90de60e1a72d1869bd8ea27dbce5b5e29f3eb1224f5"
                  alt=""
                />
              </a>
            </div>
          </div>
        </div>
      </div>

      <PokeListDetails/>

{/* Modal */}
    </>
  );
}
