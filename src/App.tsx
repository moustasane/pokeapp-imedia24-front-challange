import React from 'react';


import { PokeList } from "./components/PokeList.jsx";
import './App.css';
import { Modal } from './components/Modal.jsx';

function App() {
  return (
    <div className="App">
      <PokeList/>
      <Modal/>
    </div>
  );
}

export default App;
