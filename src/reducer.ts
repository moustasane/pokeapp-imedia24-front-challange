import { combineReducers } from "redux";
import  PokeListReducer  from './components/PokeListSlice'


const rootReducer = combineReducers({
    pokemones :   PokeListReducer
})

export default rootReducer