

import { createStore,applyMiddleware } from 'redux';
import ThunkMiddleware from "redux-thunk";
import rootReducer from '../reducer';

const enhancer = applyMiddleware(ThunkMiddleware)
export const store = createStore(rootReducer,enhancer);


