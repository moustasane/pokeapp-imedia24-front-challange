import { render, screen } from "@testing-library/react"
import { Loading } from "../components/Loading";



//Simple Example of unit testing
test('initial state loading modal should be appeard',()=>{
  


  render(
    <Loading loading={true}/>
  )
  screen.getByText("Loading...")
  expect(screen.getByText("Loading...")).toBeInTheDocument()

});